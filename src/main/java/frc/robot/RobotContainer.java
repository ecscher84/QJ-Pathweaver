// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.DriveBase;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.Shooter;
import frc.robot.utilities.AutoRoutine;
import frc.robot.utilities.CommandUtility;
/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */

public class RobotContainer {
  // The robot's subsystems and commands are defined here...
  private static final DriveBase mDriveBase = DriveBase.getInstance(); // getting instance of the drivetrain
  private static final Indexer mIndexer = Indexer.getInstance();
  private static final Shooter mShooter = Shooter.getInstance();

  SendableChooser<AutoRoutine> mAutoChooser = new SendableChooser<AutoRoutine>(); // defining autochooser smartdashboard widget

  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer() {
    CommandUtility.sendAutos();
    configureButtonBindings();
  }

  /**
   * Use this method to define your button->command mappings. Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
   * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    XboxController mController = new XboxController(0);
    Joystick mJoystick = new Joystick(1);
    
    mDriveBase.setDefaultCommand(CommandUtility.drive(() -> mController.getLeftY(), () -> mController.getRightX(), () -> mController.getLeftBumper()));
    mIndexer.setDefaultCommand(CommandUtility.index(() -> mController.getAButton(), () -> mController.getBButton()));
    mShooter.setDefaultCommand(CommandUtility.shoot(() -> mJoystick.getRawButton(1), () -> mJoystick.getY()));
  }

  public Command getAutonomousCommand() {
    return CommandUtility.getAutonomous();
  }
}
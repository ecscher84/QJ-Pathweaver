package frc.robot.utilities;

import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;


import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.ConditionalCommand;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.autoroutines.FiveBall;
import frc.robot.autoroutines.Salty1;
import frc.robot.autoroutines.Salty2;
import frc.robot.autoroutines.SixBall;
import frc.robot.autoroutines.ThreeBall1;
import frc.robot.autoroutines.ThreeBall2;
import frc.robot.autoroutines.TwoBall1;
import frc.robot.autoroutines.TwoBall2;
import frc.robot.autoroutines.TwoBall3;
import frc.robot.subsystems.DriveBase;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.Shooter;

public final class CommandUtility {
    private static final DriveBase mDriveBase = DriveBase.getInstance();
    private static final Indexer mIndexer = Indexer.getInstance();
    private static final Shooter mShooter = Shooter.getInstance();

    private static SendableChooser<AutoRoutine> mAutoChooser = new SendableChooser<AutoRoutine>();

    public static void sendAutos() {
        mAutoChooser.setDefaultOption("TwoBall1", new TwoBall1());
        mAutoChooser.addOption("TwoBall2", new TwoBall2());
        mAutoChooser.addOption("TwoBall3", new TwoBall3());
        mAutoChooser.addOption("ThreeBall1", new ThreeBall1());
        mAutoChooser.addOption("ThreeBall2", new ThreeBall2());
        mAutoChooser.addOption("FiveBall", new FiveBall());
        mAutoChooser.addOption("SixBall", new SixBall());
        mAutoChooser.addOption("Salty 1", new Salty1());
        mAutoChooser.addOption("Salty 2", new Salty2());

        SmartDashboard.putData("Autonomous Mode", mAutoChooser);
    }

    public static SequentialCommandGroup getAutonomous() {
        return new InstantCommand(
            () -> mDriveBase.resetOdometry(mAutoChooser.getSelected().getInitialPose())
        ).andThen(
            mAutoChooser.getSelected()
        );
    }

    public static CommandBase drive(DoubleSupplier fwd, DoubleSupplier rot, BooleanSupplier slow) {
        if (slow.getAsBoolean()) {
            return new RunCommand(
                () -> mDriveBase.getDrive().arcadeDrive(
                    fwd.getAsDouble() * 0.5,
                    rot.getAsDouble() * 0.5
                )).withName("slow drive");
        }
        return new RunCommand(
            () -> mDriveBase.getDrive().arcadeDrive(
                fwd.getAsDouble(),
                rot.getAsDouble()
            )).withName("drive"); 
    }

    public static CommandBase index(BooleanSupplier up, BooleanSupplier down) {
        if (up.getAsBoolean()) {
            return new InstantCommand(
                () -> mIndexer.up(),
                mIndexer
            );
        } else if (down.getAsBoolean()) {
            return new InstantCommand(
                () -> mIndexer.down(),
                mIndexer
            );
        } else {
            return new InstantCommand(
                () -> mIndexer.off(),
                mIndexer
            );
        }
    }

    public static CommandBase shoot(BooleanSupplier shoot, DoubleSupplier power) {
        return new ConditionalCommand(
            new InstantCommand(
                () -> mShooter.shoot(
                    power.getAsDouble()),
                    mShooter
            ),
            new InstantCommand(
                () -> mShooter.off(),
                mShooter
            ),
            shoot
        );
    }
}
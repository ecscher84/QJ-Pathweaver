package frc.robot.autoroutines;

import frc.robot.utilities.AutoPath;
import frc.robot.utilities.AutoRoutine;

public class ThreeBall2 extends AutoRoutine {
    private final AutoPath mPath1 = new AutoPath("paths/ThreeBall2.wpilib.json");
    private final AutoPath[] mPathList = {mPath1};

    
    public ThreeBall2() {
        addCommands(mPathList);
    }
}
package frc.robot.autoroutines;

import frc.robot.utilities.AutoPath;
import frc.robot.utilities.AutoRoutine;

public class Salty1 extends AutoRoutine {
    private final AutoPath mPath1 = new AutoPath("paths/Salty1.wpilib.json");
    private final AutoPath[] mPathList = {mPath1};

    
    public Salty1() {
        addCommands(mPathList);
    }
}
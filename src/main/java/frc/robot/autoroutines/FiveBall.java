package frc.robot.autoroutines;

import frc.robot.utilities.AutoPath;
import frc.robot.utilities.AutoRoutine;

public class FiveBall extends AutoRoutine {
    private final AutoPath mPath1 = new AutoPath("paths/FiveBall1.wpilib.json");
    private final AutoPath mPath2 = new AutoPath("paths/FiveBall2.wpilib.json");
    private final AutoPath mPath3 = new AutoPath("paths/FiveBall3.wpilib.json");
    private final AutoPath[] mPathList = {mPath1, mPath2, mPath3};
    
    public FiveBall() {
        addCommands(mPathList);
    }
}
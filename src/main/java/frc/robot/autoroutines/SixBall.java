package frc.robot.autoroutines;

import frc.robot.utilities.AutoPath;
import frc.robot.utilities.AutoRoutine;

public class SixBall extends AutoRoutine {
    private final AutoPath mPath1 = new AutoPath("paths/SixBall1.wpilib.json");
    private final AutoPath mPath2 = new AutoPath("paths/SixBall2.wpilib.json");
    private final AutoPath mPath3 = new AutoPath("paths/SixBall3.wpilib.json");
    private final AutoPath mPath4 = new AutoPath("paths/SixBall4.wpilib.json");
    private final AutoPath[] mPathList = {mPath1, mPath2, mPath3, mPath4};

    
    public SixBall() {
        addCommands(mPathList);
    }
}
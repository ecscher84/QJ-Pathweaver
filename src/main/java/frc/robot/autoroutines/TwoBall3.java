package frc.robot.autoroutines;

import frc.robot.utilities.AutoPath;
import frc.robot.utilities.AutoRoutine;

public class TwoBall3 extends AutoRoutine {
    private final AutoPath mPath1 = new AutoPath("paths/TwoBall3.wpilib.json");
    private final AutoPath[] mPathList = {mPath1};
    
    public TwoBall3() {
        addCommands(mPathList);
    }
}
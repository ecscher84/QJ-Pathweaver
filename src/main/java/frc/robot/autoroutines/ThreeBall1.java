package frc.robot.autoroutines;

import frc.robot.utilities.AutoPath;
import frc.robot.utilities.AutoRoutine;

public class ThreeBall1 extends AutoRoutine {
    private final AutoPath mPath1 = new AutoPath("paths/ThreeBall1.wpilib.json");
    private final AutoPath[] mPathList = {mPath1};
    
    public ThreeBall1() {
        addCommands(mPathList);
    }
}
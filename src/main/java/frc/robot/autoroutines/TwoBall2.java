package frc.robot.autoroutines;

import frc.robot.utilities.AutoPath;
import frc.robot.utilities.AutoRoutine;

public class TwoBall2 extends AutoRoutine {
    private final AutoPath mPath1 = new AutoPath("paths/TwoBall2.wpilib.json");
    private final AutoPath[] mPathList = {mPath1};

    
    public TwoBall2() {
        addCommands(mPathList);
    }
}
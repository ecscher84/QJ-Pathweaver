package frc.robot.autoroutines;

import frc.robot.utilities.AutoPath;
import frc.robot.utilities.AutoRoutine;

public class Salty2 extends AutoRoutine {
    private final AutoPath mPath1 = new AutoPath("paths/Salty2-1.wpilib.json");
    private final AutoPath mPath2 = new AutoPath("paths/Salty2-2.wpilib.json");
    private final AutoPath[] mPathList = {mPath1, mPath2};

    
    public Salty2() {
        addCommands(mPathList);
    }
}
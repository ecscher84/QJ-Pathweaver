package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class Shooter extends SubsystemBase {
    private CANSparkMax mLeader;
    private CANSparkMax mFollower;

    private static Shooter mInstance;

    public Shooter() {
        mLeader = new CANSparkMax(6, MotorType.kBrushless);
        mFollower = new CANSparkMax(7, MotorType.kBrushless);

        mFollower.follow(mLeader, true);
    }

    public static Shooter getInstance() {
        if (mInstance == null) {
            mInstance = new Shooter();
        }
        return mInstance;
    }

    public void shoot(double speed) {
        mLeader.set(speed);
    }

    public void off() {
        mLeader.set(0);
    }
}
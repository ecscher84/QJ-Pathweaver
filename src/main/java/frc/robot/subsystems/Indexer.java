package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class Indexer extends SubsystemBase {
    private CANSparkMax mIndexer;
    private static Indexer mInstance;

    public Indexer() {
        mIndexer = new CANSparkMax(5, MotorType.kBrushless);
    }

    public static Indexer getInstance() {
        if (mInstance == null) {
            mInstance = new Indexer();
        }
        return mInstance;
    }

    public void up() {
        mIndexer.set(1);
    }

    public void down() {
        mIndexer.set(-1);
    }

    public void off() {
        mIndexer.set(0);
    }
}